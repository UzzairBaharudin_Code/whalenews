<?php

namespace App\Http\Controllers\Backend;

use Auth;
use Session;
use App\Models\Profile;
use App\Models\User;
use App\Models\Role;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests\Backend\UsersRequest;


class UsersController extends Controller
{
    public function index()
    {
        return view('backend.users.index')->with('users', User::all());
    }

    public function create()
    {
        $role_ids = [];
        return view('backend.users.create')
        ->with('role_ids', $role_ids)
        ->with('roles', Role::all());
    }

    public function store(UsersRequest $request)
    {
        // return $request->all();
        $user = User::create([
            'name' => $request->name,
            'email' => $request->email,
            'password' => bcrypt($request->password)
        ]);
        $user->roles()->attach($request->roles);

        // If avatar field is filled in
        if ($request->hasFile('avatar')) {
            $avatar = $request->avatar;
            $avatar_new_name = time() . $avatar->getClientOriginalName();
            $avatar->move('uploads/avatars', $avatar_new_name);
        }

        $profile = Profile::create([
            'user_id' => $user->id,
            'avatar' => 'uploads/avatars/' . $avatar_new_name,
            'first_name' => $request->first_name,
            'last_name' => $request->last_name,

        ]);        

        Session::flash('success', 'User was created successfully');
        return redirect()->route('users.index');

    }

    public function edit($id)
    {   
        $user = User::find($id);
        $role_ids = [];
        foreach($user->roles as $user_role){
            $role_ids[] = $user_role->id;            
        }
        return view('backend.users.edit')
        ->with('user', User::find($id))
        ->with('roles',Role::all())
        ->with('role_ids', $role_ids);
    }

    public function update(UsersRequest $request, $id)
    {

        $user = User::find($id);
        $user->name = $request->name;
        $user->email = $request->email;
        $user->roles()->sync($request->roles);
        if (!empty($request->password)) {
            $this->validate($request, [
                'password' => 'required|alpha_num|min:8|confirmed',
            ]);
            $user->password = bcrypt($request->password);
        }

        $user->profile->first_name = $request->first_name;
        $user->profile->last_name = $request->last_name;

        if ($request->hasFile('avatar')) {
            $this->validate($request, [
                'avatar' => 'image',
            ]);
            $avatar = $request->avatar;
            $avatar_new_name = time() . $avatar->getClientOriginalName();
            $avatar->move('uploads/avatars', $avatar_new_name);
            $user->profile->avatar = '/uploads/avatars/' . $avatar_new_name;
        }

        $user->save();
        $user->profile->save();


        Session::flash('success', 'The use was update successfully');
        return redirect()->back();

    }

    public function massdelete(Request $request)
    {   
        $checked = $request->input('id', []);
        foreach($checked as $id){
            User::find($id)->delete();
        }        
        Session::flash('success', 'The user(s) was successfully deleted');
        return redirect()->route('users.index');
    }  

    public function myaccount()
    {
        return view('backend.users.profile')
            ->with('user', Auth::user());
    }
}
