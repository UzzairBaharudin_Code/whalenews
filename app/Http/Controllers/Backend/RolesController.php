<?php

namespace App\Http\Controllers\Backend;

use App\Models\Backend\Dataset;
use App\Http\Controllers\Controller;
use App\Http\Requests\Backend\RolesRequest;
use App\Models\Permission;
use App\Repositories\Backend\Roles;
use App\Models\Role;
use Auth;
use Illuminate\Http\Request;
use Session;

class RolesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Roles $roles)
    {
        return view('backend.roles.index')
            ->with('roles', $roles->all())
            ->with('user', Auth::user());
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {   
        $p_ids = [];
        $models = Dataset::with('permissions')->get();
        $permissions = Permission::orderBy('name', 'asc')->get();
        return view('backend.roles.create')
            ->with(compact('permissions', 'models', 'p_ids'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(RolesRequest $request)
    {
        Role::create([
            'name' => ucfirst($request->name),
            'key' => str_slug($request->name),
            'description' => $request->description,
        ]);
        Session::flash('success', 'The role was successfully created');
        return redirect()->route('roles.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $models = Dataset::with('permissions')->get();
        $permissions = Permission::orderBy('name', 'asc')->get();
        $p_ids = [];
        $role = Role::find($id);
        foreach ($role->permissions as $p) {
            $p_ids[] = $p->id;
        }
        return view('backend.roles.edit')
            ->with(compact('permissions', 'models', 'p_ids', 'role'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(RolesRequest $request, $id)
    {
        $role = Role::find($id);
        $role->name = ucfirst($request->name);
        $role->key = str_slug($request->name);
        $role->description = $request->description;
        $role->permissions()->sync($request->permission);
        $role->save();
        Session::flash('success', 'The role ' . $request->name . ' was sucessfully updated');
        return redirect()->back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Role::find($id)->delete();
        Session::flash('success', 'The role was successfully deleted');
        return redirect()->back();
    }

    public function massdelete(Request $request)
    {
        $checked = $request->input('id', []);
        foreach ($checked as $id) {
            Role::find($id)->delete();
        }
        Session::flash('success', 'The role was successfully deleted');
        return redirect()->route('roles.index');
    }
}
