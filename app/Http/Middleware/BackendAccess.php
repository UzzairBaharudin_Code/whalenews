<?php

namespace App\Http\Middleware;

use Auth;
use App\Models\Role;
use Closure;

class BackendAccess
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $roles = Role::pluck('name');
        if (Auth::check() && Auth::user()->hasAnyRole($roles)) {
            return $next($request);
        }
        return redirect('/login');
    }
}
