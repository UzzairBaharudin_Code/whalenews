<?php

namespace App\Models;
use App\Models\Role;
use App\Models\Profile;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable; 

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password', 'password_confirmation'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function roles(){
        return $this->belongsToMany(Role::class);
    }
    public function profile(){
        return $this->hasOne(Profile::class);
    }
    public function hasAnyRole($roles){
        return null != $this->roles()->whereIn('name', $roles)->first();
    }
    public function hasRole($role){
        return null != $this->roles()->where('name', $role)->first();
    }
}
