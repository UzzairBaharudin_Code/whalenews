<?php

namespace App\Models\Backend;

use App\Models\Permission;
use Illuminate\Database\Eloquent\Model;

class Dataset extends Model
{
    protected $guarded = [];

    public function permissions()
    {
        return $this->hasMany(Permission::class);
    }
}
