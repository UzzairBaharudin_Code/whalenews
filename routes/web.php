<?php

use Illuminate\Support\Facades\Input;
/*******************************************************************************************
 *                                  AUTH
 *******************************************************************************************/

Auth::routes();

/*******************************************************************************************
 *                                  BACKEND
 *******************************************************************************************/

Route::group(['prefix' => 'backend', 'middleware' => 'backend', 'namespace' => 'Backend'], function () {
    Route::get('/', ['uses' => 'BackendsController@index', 'as' => 'backend.index']);

    Route::group(['prefix' => 'users'], function () {
        Route::get('/myaccount', ['uses' => 'UsersController@myaccount', 'as' => 'users.myaccount']);
        Route::post('/profile/update', ['uses' => 'ProfilesController@update', 'as' => 'profile.update']);
        Route::delete('/massdelete', ['uses' => 'UsersController@massdelete', 'as' => 'users.massdelete']);
    });
    Route::resource('users', 'UsersController');

    Route::group(['prefix' => 'roles'], function () {
        Route::delete('/massdelete', ['uses' => 'RolesController@massdelete', 'as' => 'roles.massdelete']);
    });
    Route::resource('roles', 'RolesController');
    Route::group(['prefix' => 'permissions'], function () {
        Route::delete('/massdelete', ['uses' => 'PermissionsController@massdelete', 'as' => 'permissions.massdelete']);
    });
    Route::resource('permissions', 'PermissionsController');

    Route::group(['prefix' => 'models'], function () {
        Route::delete('/massdelete', ['uses' => 'DatasetsController@massdelete', 'as' => 'models.massdelete']);
    });
    Route::resource('models', 'DatasetsController');

    // Menu Management Page
    Route::get('/menu', function () {
        $menu = DB::table('tbl_menus')->orderBy('order', 'ASC')->get();
        return view('backend.menus.menu', ['menus' => $menu]);
    });

// Menu Manager
    Route::get('/managemenu', function () {
        $menu = DB::table('tbl_menus')->orderBy('order', 'ASC')->get();
        return view('backend.menus.menuManage', ['menus' => $menu]);
    });
// Menu Sorter Function
    Route::get('/order-menu', function () {
        $menu = DB::table('tbl_menus')->orderBy('order', 'ASC')->get();
        $itemID = Input::get('itemID');
        $itemIndex = Input::get('itemIndex');

        foreach ($menu as $value) {
            return DB::table('tbl_menus')
                ->where('menu_id', '=', $itemID)
                ->update(array('order' => $itemIndex));
        }
        return redirect()->route('backend.index');
    });

});
