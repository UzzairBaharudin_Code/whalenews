@extends('auth.master')
@section('content')
<div class="all-wrapper menu-side with-pattern">
	<div class="auth-box-w">
		<div class="logo-w">
			<a href="index.html">
				<img alt="" src="{{asset('img/logo.svg')}}">
			</a>
		</div>
		<h4 class="auth-header">
			Login
		</h4>
		<form method="POST" action="{{ route('login') }}">
			{{ csrf_field() }}
			<div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
				<label for="">Email</label>
				<input id="email" type="email" class="form-control" name="email" value="{{ old('email') }}" required autofocus> 
				@if ($errors->has('email'))
				<span class="help-block text-danger">
					<strong>{{ $errors->first('email') }}</strong>
				</span>
				@endif
				<div class="pre-icon os-icon os-icon-user-male-circle"></div>
			</div>
			<div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
				<label for="">Password</label>
				<input id="password" type="password" class="form-control" name="password" required> @if ($errors->has('password'))
				<span class="help-block text-danger">
					<strong>{{ $errors->first('password') }}</strong>
				</span>
				@endif
				<div class="pre-icon os-icon os-icon-fingerprint"></div>
			</div>
			<div class="buttons-w">
				<button class="btn btn-primary" type="submit">Log me in</button>
				<a class="btn btn-info" href="{{ route('register') }}">Don't have account?</a>
				<br>
				{{--  <a class="pt-5" href="{{ route('password.request') }}">Forgot Your Password?</a>  --}}
				<!-- <div class="form-check-inline">
              <label class="form-check-label"><input class="form-check-input" type="checkbox">Remember Me</label>
            </div> -->
			</div>
		</form>
	</div>
</div>