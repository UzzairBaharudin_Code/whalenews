@extends('backend.master') @section('content')
<div class="row">
  <div class="col-sm-12">
    <div class="element-wrapper">
      <h6 class="element-header">
        Edit Model
      </h6>
      <div class="element-box">
        <form action="{{route('models.update', ['id'=> $dataset->id])}}" method="post" enctype="multipart/form-data">
          {{csrf_field()}} 
          {{method_field('PUT')}}
          @include('backend.models.form')
          <div class="form-group">
            <button class="btn btn-primary" type="submit">
              Save Changes
            </button>
            <a href="{{route('models.index')}}" class="btn btn-info">Back</a>
          </div>
        </form>
      </div>
    </div>
  </div>
</div>
@stop