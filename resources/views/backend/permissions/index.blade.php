@extends('backend.master') 
@section('content')
<div class="row">
	<div class="col-sm-12">
		<div class="element-wrapper">
			<h6 class="element-header">
				Permissions
			</h6>
			<form class="form" action="{{route('permissions.massdelete')}}" method="post">
			{{csrf_field()}}
			{{method_field('DELETE')}}
			<div class="element-box">
			<div class="element-box-content">
			<div class="btn-group">
				<a href="{{route('permissions.create')}}" class="btn btn-primary">Add New Permission</a>
			</div>
			<div class="btn-group">
				<button  class="btn btn-danger" id="bulk-delete" type="submit" name="bulkdelete" value="bulkdelete"  onclick="return confirm('Are you sure you want to delete the selected record(s)?')"><i class="os-icon os-icon-ui-15"></i> Delete</button>
			</div>
			</div>
			</div>
			<div class="element-box">
				<div class="table-responsive">
					<table id="datatable" class="table table-striped">
						<thead>
							<tr>
								<th><input type="checkbox" id="selectall" class="checked" {{$permissions->count() > 0 ? '':'disabled'}} /></th>
								<th> # </th>								
								<th>Name</th>
								<th>Slug</th>
								<th>Model Key</th>								
								<th>Description</th>                               
								<th>Actions</th>
							</tr>
						</thead>
						<tbody>
							@foreach($permissions as $key=>$permission)
							<tr>
								<td><input type="checkbox" name="id[]" value="{{$permission->id}}" class="check-all"></td>
								<td>{{++$key}}</td>								
								<td>{{$permission->name}}</td>
								<td>{{$permission->slug}}</td>
								<td>{{$permission->dataset['name']}}</td>								
								<td>{{$permission->description}}</td>                               
								<td class="row-actions">
									<a href="{{route('permissions.edit',['id'=>$permission->id])}}">
										<i class="os-icon os-icon-pencil-2"></i>
									</a>
									{{--  @if(Auth::id() == $user->id)
									<a class="danger" href="{{route('permissions.destroy',['id'=>$permission->id])}}">
										<i class="os-icon os-icon-ui-15"></i>
									</a>  
									@endif--}}
								</td>
							</tr>
							@endforeach
						</tbody>
					</table>
				</div>
			</div>
			</form>
		</div>
	</div>
</div>
@endsection
@include('backend.asset-partials.datatables')
@include('backend.asset-partials.form-function')