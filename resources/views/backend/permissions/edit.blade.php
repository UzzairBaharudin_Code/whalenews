@extends('backend.master') @section('content')
<div class="row">
  <div class="col-sm-12">
    <div class="element-wrapper">
      <h6 class="element-header">
        Add New Permission
      </h6>
      <div class="element-box">
        <form action="{{route('permissions.update', ['id'=> $permission->id])}}" method="post" enctype="multipart/form-data">
          {{csrf_field()}} 
          {{method_field('PUT')}}
          @include('backend.permissions.form')
          <div class="form-group">
            <button class="btn btn-primary" type="submit">
              Save Changes
            </button>
            <a href="{{route('permissions.index')}}" class="btn btn-info">Back</a>
          </div>
        </form>
      </div>
    </div>
  </div>
</div>
@stop