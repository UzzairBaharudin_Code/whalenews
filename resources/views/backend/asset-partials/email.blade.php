@section('email-js')
<script>
// #13. EMAIL APP 

$('.more-messages').on('click', function () {
  $(this).hide();
  $('.older-pack').slideDown(100);
  $('.aec-full-message-w.show-pack').removeClass('show-pack');
  return false;
});

// CKEDITOR ACTIVATION FOR MAIL REPLY
if (typeof CKEDITOR !== 'undefined') {
  CKEDITOR.disableAutoInline = true;
  if ($('#ckeditorEmail').length) {
    CKEDITOR.config.uiColor = '#ffffff';
    CKEDITOR.config.toolbar = [
      ['Bold', 'Italic', '-', 'NumberedList', 'BulletedList', '-', 'Link', 'Unlink', '-', 'About']
    ];
    CKEDITOR.config.height = 110;
    CKEDITOR.replace('ckeditor1');
  }
}

// EMAIL SIDEBAR MENU TOGGLER
$('.ae-side-menu-toggler').on('click', function () {
  $('.app-email-w').toggleClass('compact-side-menu');
});

// EMAIL MOBILE SHOW MESSAGE
$('.ae-item').on('click', function () {
  $('.app-email-w').addClass('forse-show-content');
});

if ($('.app-email-w').length) {
  if (is_display_type('phone') || is_display_type('tablet')) {
    $('.app-email-w').addClass('compact-side-menu');
  }
}</script>
@stop