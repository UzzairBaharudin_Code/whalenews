@section('form-js')
<script>
$(document).ready(function(){

    // $(".form").on("submit", function(){
    //     if ($(".check-all:checked").length > 0){
    //         return confirm("Do you want to delete the selected item(s)?");
    //     }else{
    //         return alert("Please select a record to be deleted");
    //         exit();
    //     }

    // });

    //enable bulk delete button if more than one checkboxes are checked
    var checkBoxes = $('.check-all');
    checkBoxes.change(function(){
    $('#bulk-delete').prop('disabled', checkBoxes.filter(':checked').length < 1);   
    $('#restore').prop('disabled', checkBoxes.filter(':checked').length < 1);
    });
    $('.check-all').change();
   
    //enable bulk delete button if select all is checked
    var selectAll= $('#selectall');
    selectAll.change(function(){
    $('#bulk-delete').prop('disabled', selectAll.filter(':checked').length < 1);   
    $('#restore').prop('disabled', selectAll.filter(':checked').length < 1);  
    });
    $('#selectall').change();
    $('.check-all').change();

});

//check all checkboxes if select all is checked
 $('#selectall').on('click',function(){
        if(this.checked){
            $('.check-all').each(function(){
                this.checked = true;
            });
        }else{
             $('.check-all').each(function(){
                this.checked = false;
            });
        }
    });
    // make select all checked if all checkboxes are checked
    $('.check-all').on('click',function(){
        if($('.check-all:checked').length == $('.check-all').length){
            $('#selectall').prop('checked',true);
        }else{
            $('#selectall').prop('checked',false);
        }
    });

function permdel(){
    return confirm('Are you sure to permanently delete the records?');   
}
function del(){
    return confirm('Move this record to trash?');
}
</script>
@endsection
