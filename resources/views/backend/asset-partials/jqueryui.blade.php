@section('jqueryui')
<script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.12.1/jquery-ui.min.js"></script>
<script>
    $(function(){
      $("#menu").sortable({
        stop: function(){
          $.map($(this).find('li'), function(el) {
            var itemID = el.id;
            var itemIndex = $(el).index();
            $.ajax({
              url:'{{URL::to("backend/order-menu")}}',
              type:'GET',
              dataType:'json',
              data: {itemID:itemID, itemIndex: itemIndex},
              success: function() {  
                toastr.options.preventDuplicates = true;                
                toastr.success('The menu has been sorted successfully', 'Success');
            },
            error: function(xhr, ajaxOptions, thrownerror) { 
              toastr.options.preventDuplicates = true;                
              toastr.error('There is an error', 'Sorry!');
            }
            })
          });
        }
      });
    });
  </script>
@stop