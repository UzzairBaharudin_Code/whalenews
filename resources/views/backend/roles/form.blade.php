<div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
    <label for="name">
        Role Name
    </label>
    <input name="name" type="text" class="form-control" value="{{old('name',$role->name ?? null)}}">
    <span class="help-block text-danger">
        <strong>{{ $errors->first('name') }}</strong>
    </span>
</div>
<div class="form-group{{ $errors->has('description') ? ' has-error' : '' }}">
    <label for="description">
        Role Description
    </label>
    <input name="description" type="text" class="form-control" value="{{old('description',$role->description ?? null)}}">
    <span class="help-block text-danger">
        <strong>{{ $errors->first('description') }}</strong>
    </span>
</div>
<div class="form-group">
    <label for="description">
        Permisssions
    </label>
    <div class="row">                    
            @foreach($models as $d)
            @if($d->permissions->count() > 0) 
            <div class="col-md-3">
                <p style="font-weight:bold">{{$d->name}}</p>
                @foreach($d->permissions as $p)
                	@if(in_array($p->id, $p_ids))
                    <div class="checkbox">
                        <input type="checkbox" name="permission[]"value="{{$p->id}}" checked> {{$p->name}} ({{$p->slug}})
                    </div>
                    @else
                    <div class="checkbox">
                        <input type="checkbox" name="permission[]"value="{{$p->id}}"> {{$p->name}} ({{$p->slug}})
                    </div>
                    @endif        
                @endforeach
            </div>
            @endif
            @endforeach    
    </div>
</div>