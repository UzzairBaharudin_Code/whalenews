@extends('backend.master') 
@section('content')
<div class="row">
	<div class="col-sm-12">
		<div class="element-wrapper">
			<h6 class="element-header">
				Roles
			</h6>
			<form class="form" action="{{route('roles.massdelete')}}" method="post">
			{{csrf_field()}}
			{{method_field('DELETE')}}
			<div class="element-box">
			<div class="element-box-content">
			<div class="btn-group">
				<a href="{{route('roles.create')}}" class="btn btn-primary">Add New Role</a>
			</div>
			<div class="btn-group">
				<button  class="btn btn-danger" id="bulk-delete" type="submit" name="bulkdelete" value="bulkdelete"  onclick="return confirm('Are you sure you want to delete the selected record(s)?')"><i class="os-icon os-icon-ui-15"></i> Delete</button>
			</div>
			</div>
			</div>
			<div class="element-box">
				<div class="table-responsive">
					<table id="datatable" class="table table-striped">
						<thead>
							<tr>
								<th><input type="checkbox" id="selectall" class="checked" {{$roles->count() > 0 ? '':'disabled'}} /></th>
								<th> # </th>								
								<th>Name</th>
								<th>Key</th>
								<th>Description</th>                               
								<th>Actions</th>
							</tr>
						</thead>
						<tbody>
							@foreach($roles as $key=>$role)
							<tr>
								<td><input type="checkbox" name="id[]" value="{{$role->id}}" class="check-all"></td>
								<td>{{++$key}}</td>								
								<td>{{$role->name}}</td>
								<td>{{$role->key}}</td>
								<td>{{$role->description}}</td>                               
								<td class="row-actions">
									<a href="{{route('roles.edit',['id'=>$role->id])}}">
										<i class="os-icon os-icon-pencil-2"></i>
									</a>
									@if(Auth::id() == $user->id)
									<a class="danger" href="{{route('roles.destroy',['id'=>$role->id])}}">
										<i class="os-icon os-icon-ui-15"></i>
									</a>
									@endif
								</td>
							</tr>
							@endforeach
						</tbody>
					</table>
				</div>
			</div>
			</form>
		</div>
	</div>
</div>
@endsection
@include('backend.asset-partials.datatables')
@include('backend.asset-partials.form-function')