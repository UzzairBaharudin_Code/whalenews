@extends('backend.master') 
@section('content')
<div class="row">
	<div class="col-sm-12">
		<div class="element-wrapper">
			<h6 class="element-header">
				Users
			</h6>
			<form class="form" action="{{route('users.massdelete')}}" method="post">
			{{csrf_field()}}
			{{method_field('DELETE')}}
			<div class="element-box">
			<div class="element-box-content">
			<div class="btn-group">
				<a href="{{route('users.create')}}" class="btn btn-primary">Add New Post</a>
			</div>
			<div class="btn-group">
				<button  class="btn btn-danger" id="bulk-delete" type="submit" name="bulkdelete" value="bulkdelete"  onclick="return confirm('Are you sure you want to move the selected record(s) to trash?')"><i class="os-icon os-icon-ui-15"></i> Move To Trash</button>
			</div>
			</div>
			</div>
			<div class="element-box">

				<div class="table-responsive">
					<table id="datatable" class="table table-striped ">
						<thead>
							<tr>
								<th><input type="checkbox" id="selectall" class="checked" {{$users->count() > 0 ? '':'disabled'}} /></th>	
								<th>#</th>
								<th>Image</th>
								<th>Name</th>
                                <th>Permission</th>
								<th>Actions</th>
							</tr>
						</thead>
						<tbody>
							@foreach($users as $key=>$user)
							<tr>
								<td><input type="checkbox" name="id[]"
								value="{{$user->id}}" class="check-all"
								{{Auth::id() == $user->id ? 'disabled':''}} 								f
								></td>
								<td>{{++$key}}</td>
								<td>
									<img src="{{asset($user->profile['avatar'])}}" class="img-responsive" width="100px" />
								</td>
								<td>{{$user->name}}</td>
								<td></td>                                
								<td class="row-actions">
									<a href="{{route('users.edit',['id'=>$user->id])}}">
										<i class="os-icon os-icon-pencil-2"></i> Edit
									</a>
								</td>
							</tr>
							@endforeach
						</tbody>
					</table>
				</div>
			</div>
		</form>
		</div>
	</div>
</div>
@endsection

@include('backend.asset-partials.datatables')
@include('backend.asset-partials.form-function')