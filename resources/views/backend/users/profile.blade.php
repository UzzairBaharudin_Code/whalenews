@extends('backend.master') @section('content')

<div class="row">
    {{-- Profile Settings --}}
    <div class="col-sm-7">
        <div class="element-wrapper">
            <div class="user-profile">
                <div class="up-head-w" style="background: #005C97;  /* fallback for old browsers */
background: -webkit-linear-gradient(to right, #1c4cc3, #005C97);  /* Chrome 10-25, Safari 5.1-6 */
background: linear-gradient(to right, #1c4cc3, #005C97); /* W3C, IE 10+/ Edge, Firefox 16+, Chrome 26+, Opera 12+, Safari 7+ */
">
                    <div class="up-social">
                        <a href="#">
                            <i class="os-icon os-icon-twitter"></i>
                        </a>
                        <a href="#">
                            <i class="os-icon os-icon-facebook"></i>
                        </a>
                    </div>
                    <div class="up-main-info">
                        <div class="user-avatar-w">
                            <div class="user-avatar">
                                <img alt="" src="{{asset($user->profile->avatar)}}" class="avatar">
                            </div>
                        </div>
                        <h3 class="up-header">
                            {{$user->profile->first_name}} {{$user->profile->last_name}}
                        </h3>
                        <h5 class="up-sub-header">
                            {{$user->roles()->first()->name}}
                        </h5>
                    </div>
                    <svg class="decor" width="842px" height="219px" viewBox="0 0 842 219" preserveAspectRatio="xMaxYMax meet" version="1.1" xmlns="http://www.w3.org/2000/svg"
                        xmlns:xlink="http://www.w3.org/1999/xlink">
                        <g transform="translate(-381.000000, -362.000000)" fill="#FFFFFF">
                            <path class="decor-path" d="M1223,362 L1223,581 L381,581 C868.912802,575.666667 1149.57947,502.666667 1223,362 Z"></path>
                        </g>
                    </svg>
                </div>
            </div>
            <div class="element-box">
                <form method="post" enctype="multipart/form-data" action="{{route('profile.update')}}">
                    {{csrf_field()}}
                    <div class="form-group">
                        <div class="form-group">
                            <label for="">Profile Picture</label>
                            <input name="avatar" class="form-control-file" type="file">
                            <div class="help-block form-text text-muted form-control-feedback">
                                    Leave this blank if you dont want to change your profile picture
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                            <label for="">Name</label>
                            <input name="name" class="form-control" type="text" value="{{$user->name}}"> @if ($errors->has('name'))
                            <span class="help-block text-danger">
                                <strong>{{ $errors->first('name') }}</strong>
                            </span>
                            @endif
                        </div>
                    </div>

                    <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                        <label for=""> Email address</label>
                        <input name="email" class="form-control" type="email" value="{{$user->email}}"> @if ($errors->has('email'))
                        <span class="help-block text-danger">
                            <strong>{{ $errors->first('email') }}</strong>
                        </span>
                        @endif
                    </div>
                    <div class="row">
                        <div class="col-sm-6">
                            <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                                <label for=""> Password</label>
                                <input name="password" class="form-control" type="password"> 
                                @if ($errors->has('password'))
                                <span class="help-block text-danger">
                                    <strong>{{ $errors->first('password') }}</strong>
                                </span>
                                @endif
                                <div class="help-block form-text text-muted form-control-feedback">
                                    Leave this blank if you dont want to change your password
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="form-group">
                                <label for="">Confirm Password</label>
                                <input id="password-confirm" type="password" class="form-control" name="password_confirmation">
                            </div>
                        </div>
                    </div>
                    <div class="form-buttons-w">
                        <button class="btn btn-primary" type="submit">Save Changes</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
    {{-- User Activity --}}
    <div class="col-sm-5">
        <div class="element-wrapper">
            <div class="element-box">
                <div class="timed-activities compact">
                    <div class="element-info">
                        <div class="element-info-with-icon">
                            <div class="element-info-icon">
                                <div class="os-icon os-icon-user-male-circle2"></div>
                            </div>
                            <div class="element-info-text">
                                <h5 class="element-inner-header">
                                    Your Activity
                                </h5>
                            </div>
                        </div>
                    </div>
                    <div class="timed-activity">
                        <div class="ta-date">
                            <span>21st Jan, 2017</span>
                        </div>
                        <div class="ta-record-w">
                            <div class="ta-record">
                                <div class="ta-timestamp">
                                    <strong>11:55</strong> am
                                </div>
                                <div class="ta-activity">
                                    Created a post called
                                    <a href="#">Register new symbol</a> in Rogue
                                </div>
                            </div>
                            <div class="ta-record">
                                <div class="ta-timestamp">
                                    <strong>2:34</strong> pm
                                </div>
                                <div class="ta-activity">
                                    Commented on story
                                    <a href="#">How to be a leader</a> in
                                    <a href="#">Financial</a> category
                                </div>
                            </div>
                            <div class="ta-record">
                                <div class="ta-timestamp">
                                    <strong>7:12</strong> pm
                                </div>
                                <div class="ta-activity">
                                    Added
                                    <a href="#">John Silver</a> as a friend
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="timed-activity">
                        <div class="ta-date">
                            <span>3rd Feb, 2017</span>
                        </div>
                        <div class="ta-record-w">
                            <div class="ta-record">
                                <div class="ta-timestamp">
                                    <strong>9:32</strong> pm
                                </div>
                                <div class="ta-activity">
                                    Added
                                    <a href="#">John Silver</a> as a friend
                                </div>
                            </div>
                            <div class="ta-record">
                                <div class="ta-timestamp">
                                    <strong>5:14</strong> pm
                                </div>
                                <div class="ta-activity">
                                    Commented on story
                                    <a href="#">How to be a leader</a> in
                                    <a href="#">Financial</a> category
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script>
    function openfile(file) { window.location = "file:///" + file; }
</script>
@endsection