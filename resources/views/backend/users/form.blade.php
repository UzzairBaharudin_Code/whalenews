<div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
    <label for="name">
        Username
    </label>
    <input name="name" type="text" class="form-control" value="{{old('name',$user->name ?? null)}}">
    <span class="help-block text-danger">
        <strong>{{ $errors->first('name') }}</strong>
    </span>
</div>
<div class="form-group{{ $errors->has('first_name') ? ' has-error' : '' }}">
    <label for="first_name">
        First Name
    </label>
    <input name="first_name" type="text" class="form-control" value="{{old('first_name',$user->profile->first_name ?? null)}}">
    <span class="help-block text-danger">
        <strong>{{ $errors->first('first_name') }}</strong>
    </span>
</div>
<div class="form-group{{ $errors->has('last_name') ? ' has-error' : '' }}">
    <label for="last_name">
        Last Name
    </label>
    <input name="last_name" type="text" class="form-control" value="{{old('last_name',$user->profile->last_name ?? null)}}">
    <span class="help-block text-danger">
        <strong>{{ $errors->first('last_name') }}</strong>
    </span>
</div>
<div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
    <label for="email">
        Email
    </label>
    <input name="email" type="email" class="form-control" value="{{old('email',$user->email ?? null)}}">
    <span class="help-block text-danger">
        <strong>{{ $errors->first('email') }}</strong>
    </span>
</div>
<div class="form-group{{ $errors->has('avatar') ? ' has-error' : '' }}">
    <label for="avatar">
        Avatar
    </label>
    @if(!empty($user->profile->avatar))
    <div style="margin-top:20px;margin-bottom:20px">
        <img src="{{asset($user->profile->avatar)}}" style="width:100px" alt="">
    </div>
    @endif
    <input name="avatar" type="file" class="form-control" value="{{old('avatar',$user->profile->avatar ?? null)}}">
    <div class="help-block form-text text-muted form-control-feedback">
        Leave this blank if you dont want to change your profile picture
    </div>
    <span class="help-block text-danger">
        <strong>{{ $errors->first('avatar') }}</strong>
    </span>
</div>
<div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
    <label for="password">
        Password
    </label>
    <input name="password" type="password" class="form-control" value="">
    <div class="help-block form-text text-muted form-control-feedback">
        Leave this blank if you dont want to change your password
    </div>
    <span class="help-block text-danger">
        <strong>{{ $errors->first('password') }}</strong>
    </span>
</div>
<div class="form-group{{ $errors->has('password_confirmation') ? ' has-error' : '' }}">
    <label for="password">
        Password Confirmation
    </label>
    <input id="password_confirmation" name="password_confirmation" type="password" class="form-control" value="">
    <span class="help-block text-danger">
        <strong>{{ $errors->first('password') }}</strong>
    </span>
</div>
<div class="form-group{{ $errors->has('role') ? ' has-error' : '' }}">
    <label for="role">
        Role
    </label>
    <select id="role" name="roles[]" class="form-control">
        @foreach($roles as $role)
            @if(in_array($role->id, $role_ids))
                <option value="{{$role->id}}" selected>{{$role->name}}</option>
                @else
                <option value="{{$role->id}}">{{$role->name}}</option>
            @endif 
        @endforeach
    </select>
    <span class="help-block text-danger">
        <strong>{{ $errors->first('role') }}</strong>
    </span>
</div>