        <!-- 
          START - Mobile Menu 
          -->
        @include('backend.partials.mobile-nav')

        <!-- 
          START - Mobile Menu 
          -->

        <!--
        START - Sidebar Menu 
        -->
        <div class="desktop-menu menu-side-v2-w menu-activated-on-hover flying-menu">
          <ul class="main-menu">
            <li class="menu-sub-header">
              <span>Main</span>
            </li>
            <li class="">
              <a href="{{route('backend.index')}}">
                <div class="icon-w">
                  <div class="os-icon os-icon-window-content"></div>
                </div>
                <span>Dashboard</span></a>
            </li>            
            <li class="menu-sub-header">
              <span>Content</span>
            </li>
            <li class="has-sub-menu">
              <a href="#">
                <div class="icon-w">
                  <div class="os-icon os-icon-newspaper"></div>
                </div>
                <span>Pages</span></a>
              <div class="sub-menu-w">
                <div class="sub-menu-header">
                  Pages
                </div>
                <div class="sub-menu-i">
                  <ul class="sub-menu">
                    <li>
                      <a href="">All Pages</a>
                      <a href="">Add New Pages</a>
                      <a href="">Trashed Pages</a>                      
                    </li>                    
                  </ul>
                </div>
              </div>
            </li>
            <li class="has-sub-menu">
              <a href="#">
                <div class="icon-w">
                  <div class="os-icon os-icon-pencil-12"></div>
                </div>
                <span>Posts</span></a>
              <div class="sub-menu-w">
                <div class="sub-menu-header">
                  Posts
                </div>
                <div class="sub-menu-i">
                  <ul class="sub-menu">
                  <li>
                  <a href="#">All Posts</a>
                    </li>
                    <li>
                      <a href="#">Add New Posts</a>
                    </li>                    
                    <li>
                      <a href="#">Post Categories</a>
                    </li> 
                    </ul> 
                    <ul class="sub-menu">                    
                    <li>
                      <a href="#">Post Tags</a>
                    </li>        
                  <li><a href="#">Trashed Posts</a></li>
                  </ul>
                </div>
              </div>
            </li>
            <li class="menu-sub-header">
              <span>Users</span>
            </li>
            <li class="has-sub-menu">
              <a href="#">
                <div class="icon-w">
                  <div class="os-icon os-icon-user-male-circle"></div>
                </div>
                <span>Users</span></a>
              <div class="sub-menu-w">
                <div class="sub-menu-header">
                  Users
                </div>
                <div class="sub-menu-i">
                  <ul class="sub-menu">
                    <li>
                      <a href="{{route('users.index')}}">All Users</a>
                    </li>
                    <li>
                      <a href="{{route('users.create')}}">Add New Users</a>
                    </li>                    
                    </ul>                    
                </div>
              </div>
            </li>
            <li class="has-sub-menu">
              <a href="#">
                <div class="icon-w">
                  <div class="os-icon os-icon-tasks-checked"></div>
                </div>
                <span>Roles & Permission</span></a>
              <div class="sub-menu-w">
                <div class="sub-menu-header">
                  Users
                </div>
                <div class="sub-menu-i">
                  <ul class="sub-menu">
                    <li>
                      <a href="{{route('roles.index')}}">All Roles</a>
                    </li>
                    <li>
                      <a href="{{route('roles.create')}}">Add New Role</a>
                    </li>
                    <li>
                      <a href="{{route('permissions.index')}}">All Permissions</a>
                    </li>                    
                    </ul>                    
                </div>
              </div>
            </li>
            <li class="has-sub-menu">
              <a href="#">
                <div class="icon-w">
                  <div class="os-icon os-icon-coins-4"></div>
                </div>
                <span>Models</span></a>
              <div class="sub-menu-w">
                <div class="sub-menu-header">
                  Models
                </div>
                <div class="sub-menu-i">
                  <ul class="sub-menu">
                    <li>
                      <a href="{{route('models.index')}}">All Models</a>
                    </li>
                    <li>
                      <a href="{{route('models.create')}}">Add New Model</a>
                    </li>                                       
                    </ul>                    
                </div>
              </div>
            </li>
            <li class="menu-sub-header">
              <span>System</span>
            </li> 
            <li class="has-sub-menu">
              <a href="#">
                <div class="icon-w">
                  <div class="os-icon os-icon-ui-46"></div>
                </div>
                <span>Settings</span></a>
              <div class="sub-menu-w">
                <div class="sub-menu-header">
                  Models
                </div>
                <div class="sub-menu-i">
                  <ul class="sub-menu">
                    <li>
                      <a href="#">Site Configuration</a>
                    </li>
                    <li>
                      <a href="">Social Networks</a>
                    </li>                    
                    </ul>                    
                </div>
              </div>
            </li>                                          
          </ul>          
        </div>
        <!--
        END - Sidebar Menu
        -->