<!DOCTYPE html>
<html>

<head>
	<title></title>
	<meta charset="utf-8">
	<meta content="ie=edge" http-equiv="x-ua-compatible">
	<noscript>
	<meta http-equiv="refresh" content="300; URL=/">
	</noscript>
	<meta content="Uzzair Baharudin" name="author">
	<meta content="Exypnos CMS" name="description">
	<meta content="width=device-width, initial-scale=1" name="viewport">
	<link href="favicon.png" rel="shortcut icon">
	<link href="apple-touch-icon.png" rel="apple-touch-icon">
    <!-- Main CSS -->
    <link href="{{ asset('css/main.css') }}" rel="stylesheet">
    <link href="{{ asset('css/select2.min.css') }}" rel="stylesheet">
	<link href="{{ asset('css/toastr.min.css') }}" rel="stylesheet">
	<link href="{{ asset('css/perfect-scrollbar.min.css') }}" rel="stylesheet">
	<style type="text/css">
  	.no-fouc {display: none;}
	</style>
	@yield('datatables-css')
	@yield('ckeditor-css')
	@yield('daterangepicker-css')
    @yield('dropzone-css')
</head>
<body>
	<div class="all-wrapper menu-side solid-bg-all">

		<!-- Header -->
		@include('backend.partials.header')
		<div class="layout-w">
			<!-- Sidebar -->
			@include('backend.partials.sidebar')
			<div class="content-w">
				<!--START - Breadcrumbs-->
				<ul class="breadcrumb">
					<li class="breadcrumb-item">
						<a href="index.html">Home</a>
					</li>					
				</ul>
				<!--END - Breadcrumbs-->
				<div class="all-wrapper menu-side with-side-panel">
					<div class="layout-w">
						<div class="content-w">
							<div class="content-i">
              {{--  START - Content  --}}
								<div class="content-box">
									@yield('content')
								</div>
              {{--  END - Content  --}}
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="display-type"></div>
	</div>
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
	@yield('jqueryui')	
		<script type="text/javascript">
  	document.documentElement.className = 'no-fouc';
	</script>
	<script type="text/javascript">
	$(document).ready(function(){
		$('.no-fouc').removeClass('no-fouc');
	});
	</script>
	<script src="{{ asset('js/bootstrap.min.js') }}"></script>
    <script src="{{ asset('js/tether.min.js') }}"></script>
	<script src="{{ asset('js/select2.full.min.js') }}"></script>
	<script src="{{ asset('js/validator.min.js') }}"></script>
	<script src="{{ asset('js/perfect-scrollbar.jquery.min.js') }}"></script>
	<script src="{{ asset('js/util.js') }}"></script>
	<script src="{{ asset('js/alert.js') }}"></script>
	<script src="{{ asset('js/button.js') }}"></script>
	<script src="{{ asset('js/carousel.js') }}"></script>
	<script src="{{ asset('js/collapse.js') }}"></script>
	<script src="{{ asset('js/dropdown.js') }}"></script>
	<script src="{{ asset('js/modal.js') }}"></script>
	<script src="{{ asset('js/tab.js') }}"></script>
	<script src="{{ asset('js/tooltip.js') }}"></script>
	<script src="{{ asset('js/popover.js') }}"></script>
	<script src="{{ asset('js/toastr.min.js') }}"></script>
	@yield('form-js')
	@yield('moment-js')
	@yield('chart-js')
	@yield('ckeditor-js')
	@yield('daterangepicker-js')
	@yield('dropzone-js')
	@yield('datatables-js')
	@yield('fullcalendar-js')
	<script src="{{ asset('js/dataTables.bootstrap4.min.js') }}"></script>
    <script src="{{ asset('js/main.js?version=3.6.1') }}"></script>
	<script type="text/javascript">
		toastr.options.progressBar = true;
		toastr.options.positionClass = 'toast-top-center';
		toastr.options.closeButton = true;
		toastr.options.closeDuration = 600;
		@if(Session::has('status'))
			toastr.success("{{Session::get('status')}}");
		@endif
        @if(Session::has('success'))
            toastr.success("{{Session::get('success')}}");
        @endif
        @if(Session::has('info'))
            toastr.info("{{Session::get('info')}}");
        @endif
        @if(Session::has('deleted'))
            toastr.success("{{Session::get('deleted')}}");
        @endif
		@if(Session::has('fail'))
            toastr.error("{{Session::get('fail')}}");
        @endif
		@if(Session::has('warning'))
			toastr.warning("{{Session::get('warning')}}");
		@endif
    </script>
</body>
</html>
