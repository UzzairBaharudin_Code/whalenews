'use strict';

// ------------------------------------
// HELPER FUNCTIONS TO TEST FOR SPECIFIC DISPLAY SIZE (RESPONSIVE HELPERS)
// ------------------------------------

function is_display_type(display_type) {
  return $('.display-type').css('content') == display_type || $('.display-type').css('content') == '"' + display_type + '"';
}

function not_display_type(display_type) {
  return $('.display-type').css('content') != display_type && $('.display-type').css('content') != '"' + display_type + '"';
}
// #11. MENU RELATED STUFF

// INIT MOBILE MENU TRIGGER BUTTON
$('.mobile-menu-trigger').on('click', function () {
  $('.menu-mobile .menu-and-user').slideToggle(200, 'swing');
  return false;
});

// INIT MENU TO ACTIVATE ON HOVER
var menu_timer;
$('.menu-activated-on-hover ul.main-menu > li.has-sub-menu').mouseenter(function () {
  var $elem = $(this);
  clearTimeout(menu_timer);
  $elem.closest('ul').addClass('has-active').find('> li').removeClass('active');
  $elem.addClass('active');
});
$('.menu-activated-on-hover ul.main-menu > li.has-sub-menu').mouseleave(function () {
  var $elem = $(this);
  menu_timer = setTimeout(function () {
    $elem.removeClass('active').closest('ul').removeClass('has-active');
  }, 200);
});

// INIT MENU TO ACTIVATE ON CLICK
$('.menu-activated-on-click li.has-sub-menu > a').on('click', function (event) {
  var $elem = $(this).closest('li');
  if ($elem.hasClass('active')) {
    $elem.removeClass('active');
  } else {
    $elem.closest('ul').find('li.active').removeClass('active');
    $elem.addClass('active');
  }
  return false;
});

// #8. SELECT 2 ACTIVATION


  $(document).ready(function () {
    $('.select2').select2();
  });


// #12. CONTENT SIDE PANEL TOGGLER

$('.content-panel-toggler, .content-panel-close, .content-panel-open').on('click', function () {
  $('.all-wrapper').toggleClass('content-panel-active');
});


// #16. OUR OWN CUSTOM DROPDOWNS 
$('.os-dropdown-trigger').on('mouseenter', function () {
  $(this).addClass('over');
});
$('.os-dropdown-trigger').on('mouseleave', function () {
  $(this).removeClass('over');
});

// #17. BOOTSTRAP RELATED JS ACTIVATIONS

// - Activate tooltips
$('[data-toggle="tooltip"]').tooltip();

// - Activate popovers
$('[data-toggle="popover"]').popover();

// #19. Fancy Selector
$('.fs-selector-trigger').on('click', function () {
$(this).closest('.fancy-selector-w').toggleClass('opened');
});
