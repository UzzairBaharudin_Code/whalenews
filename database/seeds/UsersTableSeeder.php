<?php

use App\Models\Role;
use App\Models\Profile;
use App\Models\User;
use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $adminRole = Role::where('name', 'administrator')->first();
        $authorRole = Role::where('name', 'author')->first();

        User::truncate();

        $admin = User::create([
            'name' => 'Admin',
            'email' => 'uzzairwork@gmail.com',
            'password' => bcrypt('password'),
        ]);

        $author = User::create([
            'name' => 'Author',
            'email' => 'uzzairwebstudio@gmail.com',
            'password' => bcrypt('password'),
        ]);

        $adminprofile = Profile::create([
            'user_id' => $admin->id,
            'avatar' => 'uploads/avatars/avatar1.jpg',
            'first_name' => 'Uzzair',
            'last_name' => 'Baharudin',

        ]);

        $admin->roles()->attach($adminRole);
        $author->roles()->attach($authorRole);
    }
}
